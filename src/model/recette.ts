export interface Recette {
    id: string;
    name: string;
    creatorId: string;
    category: string;
    quantite: string;
    calories: number;
    ingredients: string[];
    preparation : string[];
    }