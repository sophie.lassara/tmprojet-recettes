import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { restaurantOutline, homeOutline, pizzaOutline, iceCreamOutline } from 'ionicons/icons';
import Tab1 from './pages/Tab1';
import Tab2 from './pages/Tab2';
import Tab3 from './pages/Tab3';
import Tab4 from './pages/Tab4';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          <Route path="/accueil" component={Tab1} exact={true} />
          <Route path="/entrees" component={Tab2} exact={true} />
          <Route path="/plats" component={Tab3} exact={true}/>
          <Route path="/desserts" component={Tab4} />
          <Route path="/" render={() => <Redirect to="/accueil" />} exact={true} />
        </IonRouterOutlet>
        <IonTabBar slot="bottom">
          <IonTabButton tab="accueil" href="/accueil">
            <IonIcon icon={homeOutline} />
            <IonLabel>Accueil</IonLabel>
          </IonTabButton>
          <IonTabButton tab="entrees" href="/entrees">
            <IonIcon icon={restaurantOutline} />
            <IonLabel>Entrées</IonLabel>
          </IonTabButton>
          <IonTabButton tab="plats" href="/plats">
            <IonIcon icon={pizzaOutline} />
            <IonLabel>Plats</IonLabel>
          </IonTabButton>
          <IonTabButton tab="desserts" href="/desserts">
            <IonIcon icon={iceCreamOutline} />
            <IonLabel>Desserts</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
