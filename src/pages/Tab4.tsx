import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonAvatar, IonChip, IonLabel } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab4.css';

const Tab3: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="md">
          <IonTitle >
            <IonChip mode="ios" outline color="primary">
              <IonAvatar>
                <img src="../../assets/coeur_coulant.jpg" alt="coulant_choco"/>
              </IonAvatar>
              <IonLabel class="title">Les recettes de Sophie</IonLabel>
            </IonChip>
          </IonTitle>
        </IonToolbar>
        <IonToolbar mode="ios">
          <IonTitle size="small">Les desserts</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar mode="md">
            <IonTitle>
              <IonChip mode="ios" outline color="primary">
                <IonAvatar>
                  <img src="../../assets/coeur_coulant.jpg" alt="coulant_choco"/>
                </IonAvatar>
                <IonLabel class="title">Les recettes de Sophie</IonLabel>
              </IonChip>
            </IonTitle>
          </IonToolbar>
          <IonToolbar mode="ios">
            <IonTitle size="small">Les desserts</IonTitle>
          </IonToolbar>
        </IonHeader>
        <ExploreContainer name="Desserts" filtreCategory="Dessert" />
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
