import React, {useState} from "react";
import { Recette } from "../model/recette";
import {IonModal, IonButton, IonList, IonItem, IonLabel } from "@ionic/react";
import Event from "../components/Calendar";
import ModalRecipe from "../components/ModalRecipe";

interface Props {
  recettes: Recette[];
  filtre: string;
  filtreCategory: string;
}

const Recettes = ({ recettes, filtre, filtreCategory }: Props) => {
  const [recette, setRecette] = useState<Recette>();

  return (
    <>
    {recette ? (
      <>
        <IonModal isOpen={recette !== undefined} > 
          <ModalRecipe title={recette.name} qte={recette.quantite} text1={recette.ingredients} text2={recette.preparation} ></ModalRecipe>
          <IonButton onClick={() => setRecette(undefined)}>Fermer recette</IonButton>
        </IonModal> 
      </> 
    ):(
      <IonList>
        {recettes.filter((recette) => recette.name.toLowerCase().includes(filtre.toLowerCase()) && recette.category.includes(filtreCategory)).map((recette) => (
          <IonItem>
              <IonButton fill="clear" onClick = {() => setRecette(recette)}>
                <IonLabel class="ion-text-wrap">{recette.name}</IonLabel>
              </IonButton>
              <Event title={recette.name}/>   
          </IonItem>
        ))}
      </IonList>
    )}
    </>
  );
};


export default Recettes;