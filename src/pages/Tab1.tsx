import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonAvatar, IonChip, IonLabel } from '@ionic/react';
import './Tab1.css';

const Tab1: React.FC = () => {

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar mode="md">
          <IonTitle >
            <IonChip mode="ios" outline color="primary">
              <IonAvatar>
                <img src="../../assets/coeur_coulant.jpg" alt="coulant_choco"/>
              </IonAvatar>
              <IonLabel class="title">Les recettes de Sophie</IonLabel>
            </IonChip>
          </IonTitle>
        </IonToolbar>
        <IonToolbar mode="ios">
          <IonTitle size="small">Accueil</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar mode="md">
            <IonTitle>
              <IonChip mode="ios" outline color="primary">
                <IonAvatar>
                  <img src="../../assets/coeur_coulant.jpg" alt="coulant_choco"/>
                </IonAvatar>
                <IonLabel class="title">Les recettes de Sophie</IonLabel>
              </IonChip>
            </IonTitle>
          </IonToolbar>
          <IonToolbar mode="ios">
            <IonTitle size="small">Accueil</IonTitle>
          </IonToolbar>
        </IonHeader>
        <p>Tu es sur la bonne page pour trouver tous les bons petits plats que mamie nous fait 
          de l'entrée/apéritif au dessert ! Pour cela rien de plus simple, rends-toi sur les onglets en bas pour 
          trouver la recette qui te convient ! Ajoute la recette à ton calendrier pour te créer un planning des repas en cliquant sur l'icône à droite.
        </p><br></br>
        <p>Cette application a été conçue dans le cadre du projet pour le cours de Technologies mobiles 
          de 2ème année de l'ENSAI (2019/2020). Le code de cette application est disponible sur <a href="https://gitlab.com/sophie.lassara/tmprojet-recettes">gitlab</a>.
        </p>
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
