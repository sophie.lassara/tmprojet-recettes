import React from 'react';
import {IonHeader, IonContent, IonToolbar, IonTitle, IonLabel, IonList, IonItem} from '@ionic/react';

interface MyModalProps {
    title: string;
    qte: string;
    text1: string[];
    text2: string[];
}

const ModalRecipe: React.FC<MyModalProps> = ({ title , qte, text1 , text2}) => {

    return (
        <>
        <IonHeader>
          <IonToolbar color="primary" mode="md">
            <IonTitle><IonLabel class="ion-text-wrap">{title}</IonLabel></IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonContent className="ion-padding">
          <p>{qte}</p>
          <h2>Ingrédients : </h2>
            <IonList>
              {text1.map((ingredient)=> (
              <IonItem>
                <IonLabel class="ion-text-wrap" color="secondary"> 
                  {ingredient}
                </IonLabel>  
              </IonItem>
              ))}
            </IonList>
          <h2>Préparation : </h2>
            <IonList>
              {text2.map((etape)=> (
              <IonItem>
                <IonLabel class="ion-text-wrap" color="secondary"> 
                  {etape}
                </IonLabel>  
              </IonItem>
              ))}
            </IonList>
      </IonContent>
      </>  
    );
  
}
    
export default ModalRecipe;
