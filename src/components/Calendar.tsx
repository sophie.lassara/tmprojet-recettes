import React from "react";
import { IonButton, IonIcon } from "@ionic/react";
import { calendarOutline } from 'ionicons/icons';
import { Calendar } from '@ionic-native/calendar';
/*import { LocalNotifications } from '@ionic-native/local-notifications';*/

interface PropsEvent {
    title: string;
  }

const Event = ({title}: PropsEvent) => {
      return (
        <IonButton
            slot="end"
            fill="clear"
            onClick={() => {
                Calendar.createEventInteractively(title, '', '', new Date(), new Date())
                .then(() => {console.log("Accès au calendrier");
                }).catch(() => {console.log("Pas accès au calendrier");});
          }}
        >
            <IonIcon slot="icon-only" icon={calendarOutline} />
        </IonButton>
      );
    };
    
export default Event;